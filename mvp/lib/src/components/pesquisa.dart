import 'package:flutter/material.dart';

class Pesquisa extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
        borderRadius: BorderRadius.all(
          Radius.circular(10.0),
        ),
        elevation: 5,
        child: TextField(
            style: TextStyle(color: Colors.black, fontSize: 16),
            cursorColor: Colors.pinkAccent,
            decoration: InputDecoration(
                contentPadding:
                    EdgeInsets.symmetric(horizontal: 20.0, vertical: 14),
                suffixIcon: Material(
                    child: Icon(
                  Icons.search,
                  color: Colors.black,
                )),
                border: InputBorder.none,
                hintText: "Buscar")));
  }
}

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class CardRestaurante extends StatefulWidget {
  @override
  _CardRestauranteState createState() => _CardRestauranteState();
}

class _CardRestauranteState extends State<CardRestaurante> {
  var cardText = TextStyle(fontSize: 16.0, fontWeight: FontWeight.bold);
  var dados;
  var nome, imagem, avaliacao;

  _getData() async {
    final url = "http://192.168.1.2/flutter/restaurantes/restaurantes.php";
    final response = await http.get((Uri.parse(url)));
    final map = json.decode(response.body);
    final itens = map["result"];
    this.dados = itens;
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    _getData();
  }

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      child: Stack(
        children: <Widget>[
          Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width * 0.9,
            child: new ListView.builder(
                itemCount: this.dados != null ? this.dados.length : 0,
                itemBuilder: (context, i) {
                  final item = this.dados[i];
                  return new Container(
                      height: MediaQuery.of(context).size.height * 0.25,
                      width: MediaQuery.of(context).size.width * 0.9,
                      margin: EdgeInsets.only(
                          bottom: MediaQuery.of(context).size.height * 0.02),
                      child: Stack(
                        children: <Widget>[
                          Image(
                            image: NetworkImage(item['imagem']),
                            height: MediaQuery.of(context).size.height * 0.25,
                            width: MediaQuery.of(context).size.width * 0.9,
                            fit: BoxFit.fill,
                          ),
                          Positioned(
                            left: 0.0,
                            bottom: 0.0,
                            width: MediaQuery.of(context).size.width * 0.9,
                            height: 70.0,
                            child: Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                              ),
                            ),
                          ),
                          Positioned(
                            left: 10.0,
                            bottom: 10.0,
                            right: 10.0,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      item['nome'],
                                      style: TextStyle(
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.bold,
                                          color: Colors.black),
                                    ),
                                    Text(
                                      item['descricao'],
                                      style: TextStyle(
                                          fontSize: 16.0, color: Colors.black),
                                    ),
                                    Row(
                                      children: <Widget>[
                                        Icon(
                                          Icons.star,
                                          color: Colors.pinkAccent,
                                          size: 16.0,
                                        ),
                                        SizedBox(
                                          width: 4.0,
                                        ),
                                        Text(
                                          (item['avaliacao']).toString(),
                                          style: TextStyle(color: Colors.black),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ],
                            ),
                          ),
                        ],
                      ));
                }),
          )
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:mvp/src/pages/login_page.dart';
import 'package:mvp/src/pages/public_page.dart';
import 'pages/home_page.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "MVP",
      theme: ThemeData(primaryColor: Colors.white),
      home: HomePage(),
      debugShowCheckedModeBanner: false,
    );
  }
}

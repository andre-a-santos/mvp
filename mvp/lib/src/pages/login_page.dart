import 'package:flutter/material.dart';
import 'package:mvp/src/components/botao.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'home_page.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  bool _toggleVisibility = true;
  var emailtxt = new TextEditingController();
  var senhatxt = new TextEditingController();
  var dados;
  var seguro = true;
  // var conf = Configuracoes.url;

  final urlImage = 'https://cdn.wallpapersafari.com/70/4/EXJpaQ.jpg';
  Widget _email() {
    return TextFormField(
      controller: emailtxt,
      cursorColor: Colors.pinkAccent,
      decoration: InputDecoration(
        hintText: "e-mail",
        hintStyle: TextStyle(
          color: Color(0xFFBDC2CB),
          fontSize: 16.0,
        ),
        contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 14),
        border: InputBorder.none,
      ),
    );
  }

  Widget _senha() {
    return TextFormField(
      controller: senhatxt,
      cursorColor: Colors.pinkAccent,
      decoration: InputDecoration(
        hintText: "senha",
        hintStyle: TextStyle(
          color: Color(0xFFBDC2CB),
          fontSize: 16.0,
        ),
        contentPadding: EdgeInsets.symmetric(horizontal: 20.0, vertical: 14),
        border: InputBorder.none,
        suffixIcon: IconButton(
          onPressed: () {
            setState(() {
              _toggleVisibility = !_toggleVisibility;
            });
          },
          icon: _toggleVisibility
              ? Icon(Icons.visibility_off)
              : Icon(Icons.visibility),
        ),
      ),
      obscureText: _toggleVisibility,
    );
  }

  Future<String> Login(String usuario, String senha) async {
    var response = await http.get(
        Uri.parse(
            "http://localhost/flutter/usuarios/login.php?usuario=${usuario}&senha=${senha}"),
        headers: {"Accept": "application/json"});
    var obj = json.decode(response.body);
    var msg = obj["message"];
    if (msg == "Dados incorretos!") {
      print('Usuário inválido');
    } else {
      dados = obj['result'];
    }
    return dados;
  }

  VerificarDados(String usuario, String senha) {
    if (dados[0]['nome'] != null) {
      var route = new MaterialPageRoute(
        builder: (BuildContext context) => HomePage(),
      );
      Navigator.of(context).push(route);
    } else {
      print('Usuário inválido');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(urlImage),
            fit: BoxFit.cover,
          ),
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              SizedBox(
                height: 20.0,
              ),
              Card(
                elevation: 5.0,
                child: Padding(
                  padding: EdgeInsets.all(20.0),
                  child: Column(
                    children: <Widget>[
                      _email(),
                    ],
                  ),
                ),
              ),
              Card(
                elevation: 5.0,
                child: Padding(
                  padding: EdgeInsets.all(20.0),
                  child: Column(
                    children: <Widget>[
                      _senha(),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 30.0,
              ),
              GestureDetector(
                onTap: () {
                  Login(emailtxt.text, senhatxt.text);
                  var route = new MaterialPageRoute(
                    builder: (BuildContext context) => HomePage(),
                  );
                  Navigator.of(context).push(route);
                },
                child: Button(btnText: "Entrar"),
              ),
              Container(
                padding: EdgeInsets.all(25.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Esqueci minha senha",
                      style: TextStyle(
                        fontSize: 15.0,
                        color: Colors.white,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

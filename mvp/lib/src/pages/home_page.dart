import 'package:flutter/material.dart';
import 'package:mvp/src/components/card_restaurantes.dart';
import 'package:mvp/src/components/pesquisa.dart';
import 'package:mvp/src/pages/login_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        shadowColor: Colors.white,
        foregroundColor: Colors.black,
        backgroundColor: Colors.white,
        title: Text(
          "Olá, você está neste endereço?",
          style: TextStyle(fontSize: 17.0),
        ),
      ),
      drawer: Drawer(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Container(
              padding: EdgeInsets.only(
                  bottom: (MediaQuery.of(context).size.height * 0.02)),
              child: RaisedButton(
                color: Colors.pinkAccent,
                onPressed: () {
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) => LoginPage()),
                      (Route<dynamic> route) => false);
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    RotatedBox(
                      quarterTurns: 2,
                      child: Icon(Icons.exit_to_app, color: Colors.white),
                    ),
                    Text(
                      "   Sair",
                      style: TextStyle(color: Colors.white),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      body: ListView(
        padding: EdgeInsets.all(20),
        children: <Widget>[
          Pesquisa(),
          SizedBox(height: 20),
          Column(
            children: <Widget>[CardRestaurante()],
          )
        ],
      ),
    );
  }
}

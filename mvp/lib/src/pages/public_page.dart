import 'package:flutter/material.dart';

class PublicPage extends StatefulWidget {
  @override
  _PublicPageState createState() => _PublicPageState();
}

class _PublicPageState extends State<PublicPage> {
  Widget build(BuildContext context) {
    final urlImage = 'https://cdn.wallpapersafari.com/70/4/EXJpaQ.jpg';
    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: NetworkImage(urlImage),
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }
}

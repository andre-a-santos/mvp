class RestaurantesModel {
  final String id;
  final String nome;
  final String avaliacao;

  RestaurantesModel({
    required this.id,
    required this.nome,
    required this.avaliacao,
  });
}

final restaurantes = [
  RestaurantesModel(id: "1", nome: "Restaurante sem futuro", avaliacao: "5"),
  RestaurantesModel(id: "2", nome: "Pizzaria sem futuro", avaliacao: "2"),
];
